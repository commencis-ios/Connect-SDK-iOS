Pod::Spec.new do |s|
    s.name = 'Connect-SDK-iOS'
    s.version = '3.12.0'
    s.summary = 'Analytics and Customer Engagement Tool'
    s.homepage = 'https://gitlab.com/commencis-ios/Connect-SDK-iOS/'

    s.author = { 'iOS Developers' => 'iosdevelopers@commencis.com' }
    s.license = { :type => 'Commencis', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :git => 'https://gitlab.com/commencis-ios/Connect-SDK-iOS.git', :tag => s.version.to_s }
    s.preserve_paths = ['upload_dsym.sh']

    s.ios.deployment_target = '12.0'
    s.ios.vendored_frameworks = 'release/AppConnect.xcframework'
end
